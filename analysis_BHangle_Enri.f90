!--------------------------------------------------------------------------!
! The Phantom Smoothed Particle Hydrodynamics code, by Daniel Price et al. !
! Copyright (c) 2007-2020 The Authors (see AUTHORS)                        !
! See LICENCE file for usage and distribution conditions                   !
! http://phantomsph.bitbucket.io/                                          !
!--------------------------------------------------------------------------!
module analysis
!
! Analysis routine to crudely estimate the change in angular momentum of BH
! when using a fixed potential
!
! :References: Gerosa et al. 2020
!
! :Owner: Bec Nealon and Enrico Ragusa
!
! :Runtime parameters: None
!
! :Dependencies: disc_analysis
!

 use discanalysisutils, only:disc_analysis

 implicit none
 character(len=20), parameter, public :: analysistype = 'BHangle'
 public :: do_analysis
 integer, parameter :: nr = 300

 private

contains

subroutine do_analysis(dumpfile,num,xyzh,vxyz,particlemass,npart,time,iunit)
 use part,         only:massoftype,igas,xyzmh_ptmass,vxyz_ptmass,nptmass
 use vectorutils,  only:cross_product3D,rotatevec
 use units,        only:utime
 use physcon,      only:pi,years,pc
 use extern_lensethirring, only:sin_spinangle,cos_spinangle,blackhole_spin
 use externalforces,       only:mass1
 use io,           only : fatal
 character(len=*), intent(in)    :: dumpfile
 integer,          intent(in)    :: num,npart,iunit
 real,             intent(inout)    :: xyzh(:,:),vxyz(:,:)
 real,             intent(in)    :: particlemass,time
 integer    :: ii,iline,i,ierr,dint(nr),ninbin(nr),i_LT
 integer, parameter :: iparams = 10
 character(len=20) :: discprefix
 logical    :: iexist,ifile
 real, save :: t_old = -1.,ctheta_pred,R_star(3),raw_theta_old
 real       :: sum_dJdt(3),pmass,fixed_BH(3),Li(3),r
 real       :: xi(3),vi(3),dJdt_mag,dJdt_Junit,dJdt_unit(3),L_star(3),dt
 real       :: L_star_unit(3),temp(3),test,R_starnew(3)
 real       :: L_tot(3),fixed_BH_unit(3),vec_mag
 real       :: J(3),rotate_about_z,rotate_about_y,J_unit(3)
 real	    :: dcdt_ave_sum,alpha,eta,rref,honrref,nuref,sigma(nr),unitl(3,nr),L_disc(3,nr),rad(nr)
 real	    :: R_in,R_out,d(nr),G,M_star,time_here,term(3)
 character(len=40) :: filename
 real       :: dcdt_inst_sum,sigmai,Li_mag,angle,dr,testing(3),JcrossL(3,nr),R_LT,scaled_rad(nr)
 real       :: dcdt_ave(nr),dcdt_LHS(nr),total_dJdt(3,nr),dcdt_inst(nr),dcdt_LHS_sum,scaled_r
 logical    :: move_star
 real	    :: L_bin_mag,L_bin(3),dcdt_ave_LHS(3),LHS_bin(3,nr),LHS_bin_mag,dcdt_LHS_bin,dJdt_J,t_align

 ! initialise
 if (t_old < 0.0) then
   t_old = time
   ctheta_pred = cos_spinangle
   R_star = xyzmh_ptmass(1:3,1)
   raw_theta_old = 0.
 endif
 dt = time - t_old

 ! parameters that need to be fed in
 alpha = 0.1
 blackhole_spin = 0.9
 angle = 80.
 sin_spinangle = sin(angle*pi/180.)
 cos_spinangle = cos(angle*pi/180.)
 R_LT = 22.83

 ! Calculate/set some common variables
 call cross_product3D(xyzmh_ptmass(1:3,1),vxyz_ptmass(1:3,1),L_star)
 L_star = xyzmh_ptmass(4,1)*L_star
 vec_mag = sqrt(dot_product(L_star,L_star))
 L_star_unit = L_star/vec_mag

 fixed_BH = (/blackhole_spin*sin_spinangle,0.,blackhole_spin*cos_spinangle/)
 fixed_BH_unit = fixed_BH/sqrt(dot_product(fixed_BH,fixed_BH))

 G = 1.0
 M_star = 1.0
 pmass = massoftype(igas)

 ! Get some properties from the discparams file
 iline = index(dumpfile,'_')
 discprefix = dumpfile(1:iline-1)
 inquire(file=trim(discprefix)//'.discparams', exist=ifile)
 if (ifile) then
    call read_discparams(trim(discprefix)//'.discparams',R_in,R_out,honrref,rref,iparams,ierr)
    if (ierr /= 0) call fatal('analysis','could not open/read .discparams file')
 endif

 ! For the bins
 dr = (R_out - R_in)/real(nr)

 !--- Calculate Equation 37, particle by particle, but also saved in bins
 ! NB: G=c=1 because iexternalforce=9,11
 L_tot = 0.
 ninbin = 0
 total_dJdt = 0.
 JcrossL = 0.
 do ii=1,npart
   xi = xyzh(1:3,ii)
   vi = vxyz(1:3,ii)
   r = sqrt(dot_product(xi,xi))
   call cross_product3D(xi,vi,Li)
   Li = pmass*Li
   call cross_product3D(fixed_BH,Li,term)
   ! which bin is this in?
   i = int((r - R_in)/dr + 1)
   if (i < 1 .or. i > nr) cycle
   JcrossL(:,i) = JcrossL(:,i) + term
   total_dJdt(:,i) = total_dJdt(:,i) + 2.*term/(r**3)
   ninbin(i) = ninbin(i) + 1
   L_tot = L_tot + Li
 enddo
 sum_dJdt(1) = sum(total_dJdt(1,:))
 sum_dJdt(2) = sum(total_dJdt(2,:))
 sum_dJdt(3) = sum(total_dJdt(3,:))
 ! this is the complete equation 37, verified it is the same summing by bins or particles
 !sum_dJdt = -2.0*sum_dJdt

 ! calculate the magnitude of dJdt
 dJdt_mag = sqrt(dot_product(sum_dJdt,sum_dJdt))
 !-- (dJ/dt)_unit
 dJdt_unit=sum_dJdt/dJdt_mag
 !-- dJ/dt*J_unit
 dJdt_Junit=dot_product(sum_dJdt,fixed_bh_unit)
 !-- dJ_unit/dt*L_star
 dJdt_J=-dot_product(sum_dJdt-dJdt_Junit*fixed_bh_unit,L_star_unit)&
                         /(blackhole_spin)
 !--- Calculate LHS of Equation 38
 do i = 1,nr
    dcdt_LHS(i) = dot_product(total_dJdt(:,i)/blackhole_spin,L_star_unit)
 enddo
 dcdt_LHS_sum = sum(dcdt_LHS)    ! this is the complete LHS of 38

 !--- Preliminary calculations to calculate the RHS of 38
 ! Equation 10
 nuref = honrref**2 * sqrt(rref)
 ! Equation 12
 eta = 2.*(1. + 7.*alpha**2)/(alpha**2*(4. + alpha**2))

 !--- Calculate the RHS of 38 particle by particle, but also saved in bins
 dcdt_inst = 0.
 do ii = 1,npart
   xi = xyzh(1:3,ii)
   vi = vxyz(1:3,ii)
   r = sqrt(dot_product(xi,xi))
   scaled_r = r!/R_LT
   call cross_product3D(xi,vi,Li)
   Li = pmass*Li
   Li_mag = sqrt(dot_product(Li,Li))
   sigmai = Li_mag/(scaled_r**0.5)
   call cross_product3D(fixed_BH_unit,Li/Li_mag,term) !--this accounts for 1/|J|
   ! which bin is this in?
   i = int((r - R_in)/dr + 1)
   if (i < 1 .or. i > nr) cycle
   !-- Note that if we are summing up on individual contribution of particles
   !-- We do not need R*2Pi, which makes the power R**2.5
   dcdt_inst(i) = dcdt_inst(i) + 2.*dot_product(term,L_star_unit)*sigmai/(scaled_r**2.5)
 enddo
 !-- Here the renorm factor is meant to transform sigma --> Sigma
 dcdt_inst = -dcdt_inst!*(-2.*pi*sqrt(nuref*alpha*eta/blackhole_spin))
 ! this is the complete RHS of 38, summed over particles, also verified
 dcdt_inst_sum = sum(dcdt_inst)

 !--- Calculate the RHS of 38 but using averaged bins from the start

 ! Need sigma and L_unit as a function of R, call the disc analysis
 call disc_analysis(xyzh,vxyz,npart,pmass,time_here,nr,R_in,R_out,G,M_star,&
                     d,d,d,d,d,d,rad,d,sigma,unitl(1,nr),unitl(2,:),unitl(3,:),&
                     L_disc(1,:),L_disc(2,:),L_disc(3,:),d,dint,&
		     .true.,xyzmh_ptmass,vxyz_ptmass,nptmass)

 scaled_rad = rad!/R_LT !for this simulation

 dcdt_ave = 0.
 do i = 1,nr
    call cross_product3D(fixed_BH_unit,unitl(:,i),term) !-- Accounts for 1/|J|
    dcdt_ave(i) = dcdt_ave(i) + 2.*dot_product(term,L_star_unit)*sigma(i)/(scaled_rad(i)**1.5)
 enddo

 dcdt_ave = -dcdt_ave*2.*pi*dr!(-2.*pi*sqrt(nuref*alpha*eta/blackhole_spin))
 dcdt_ave_sum = sum(dcdt_ave)

 !--- To complete the set, now calculate LHS of 38 using bins only
 dcdt_ave_LHS = 0.
 do i = 1,nr
    L_bin_mag = sigma(i)*sqrt(G*M_star*rad(i))  ! M_star here is actually mass of BH
    L_bin = L_bin_mag*unitl(:,i)
    call cross_product3D(fixed_BH,L_bin,term)
    LHS_bin(:,i) = 2.0*term/(rad(i)**2)*2.*pi*dr
 enddo

 dcdt_ave_LHS(1) = sum(LHS_bin(1,:))
 dcdt_ave_LHS(2) = sum(LHS_bin(2,:))
 dcdt_ave_LHS(3) = sum(LHS_bin(3,:)) !across all R bins
 vec_mag = sqrt(dot_product(dcdt_ave_LHS,dcdt_ave_LHS))
 dcdt_LHS_bin = -dot_product(dcdt_ave_LHS/blackhole_spin,L_star_unit)

 !-- calculating t_align
 !-- Where is R_LT
 i_LT = int((R_LT - R_in)/dr + 1)
 t_align=1./(3.*pi*sigma(i_LT)*nuref)*sqrt(blackhole_spin*alpha*nuref/eta)
 !--- Write everything out

 ! LHS and RHS of Eq 38 as a function of radius
 write(filename,"(a5,i5.5)") 'Eq38_',num
 open(unit=iunit,file=filename,status="replace")
 write(iunit,"('#',7(1x,'[',i2.2,1x,a11,']',2x))") &
              1,'radius', &
              2,'LHS', &
              3,'RHS ave', &
	      4,'RHS ins', &
              5,'JxL(1)', &
	      6,'JxL(2)', &
  	      7,'JxL(3)'
 do i = 1,nr
  write(iunit,'(7(es18.10,1X))') rad(i), -dcdt_LHS(i), dcdt_ave(i),&
			 dcdt_inst(i), JcrossL(1:3,i)
 enddo
 close(iunit)

 ! Write the sums out as a function of time
 filename='BH_angle.out'
 inquire(file=filename,exist=iexist)
 if (.not.iexist .or. time < tiny(time)) then
   open(unit=iunit,file=filename,status="replace")
   write(iunit,"('#',11(1x,'[',i2.2,1x,a11,']',2x))") &
              1,'time', &
              2,'unit_dJdt(1)',&
              3,'unit_dJdt(2)',&
              4,'unit_dJdt(3)',&
              5,'dJ_u/dt*L_star', &
              6,'dJ/dt/|J|*L_star', &
              7,'RHS 38 ave',&
              8,'RHS 38 inst',&
	      9,'LHS bin',&
          10,'R_LT',&
          11,'t_align'
 else
   open(unit=iunit,file=filename,status="old",position="append")
 endif
 write(iunit,'(11(es18.10,1X))') time,dJdt_unit(:),dJdt_J, &
                      -dcdt_LHS_sum, dcdt_ave_sum, dcdt_inst_sum,&
                      dcdt_LHS_bin,rad(i_LT),t_align
 close(unit=iunit)

 write(*,*) "End of analysis BHangle"

 return





 !--------------OLD STUFF-------------


 ! rotate the disc and star *as though* the BH has moved for next step
 ! rotation is done in 2 stages: we rotate around the z axis
 ! to be coincident with the x axis, then rotate about the y axis
 ! we look for the rotation required to move the new J
 ! back to it's original fixed position

J = fixed_BH + sum_dJdt*dt !where J "should" be
temp = (/J(1),J(2),0./)
vec_mag = sqrt(dot_product(temp,temp))

! rotation in the x-y plane, lines J up with x-axis
if (vec_mag < tiny(vec_mag)) then
  rotate_about_z = -acos(dot_product((/1.,0.,0./),temp))*sign(1.0,temp(2))
else
  rotate_about_z = -acos(dot_product((/1.,0.,0./),temp/vec_mag))*sign(1.0,temp(2))
endif

call rotatevec(J,(/0.,0.,1.0/),rotate_about_z)
vec_mag = sqrt(dot_product(J,J))
J_unit = J/vec_mag

! rotation in z-x plane, lines J up with fixed_BH position
 test = dot_product((fixed_BH_unit),J_unit)
 if ((test - 1.0) > tiny(test)) then
    rotate_about_y = 0.
 else
    rotate_about_y = -acos(dot_product((fixed_BH_unit),J_unit))
 endif

! this is required if the BH is retrograde
if (J_unit(1) > 0. .and. cos_spinangle < 0.) rotate_about_y = -rotate_about_y

call rotatevec(J,(/0.,1.,0./),rotate_about_y)

! quickly check that there has been no error
 if (isnan(rotate_about_z) .or. isnan(rotate_about_y)) then
    call fatal('analysis_BHangle','one of the rotation angles is not defined')
 endif

! now we have angles, rotate the star
 if (move_star) call rotate_it(xyzmh_ptmass(1:3,1),vxyz_ptmass(1:3,1),rotate_about_z,rotate_about_y)

! rotate the gas particles
 do ii = 1,npart
   xi = xyzh(1:3,ii)
   vi = vxyz(1:3,ii)

   call rotate_it(xi,vi,rotate_about_z,rotate_about_y)

   xyzh(1:3,ii) = xi
   vxyz(1:3,ii) = vi
 enddo

 ! update values for next time-step
 t_old = time
 R_star = R_starnew

end subroutine do_analysis

  ! Rotate position and velocity vectors about z and y axis

subroutine rotate_it(pos,vel,z_angle,y_angle)
  use vectorutils,  only:rotatevec
    real, intent(inout) :: pos(3),vel(3)
    real, intent(in)    :: z_angle,y_angle

  call rotatevec(pos,(/0.,0.,1.0/),z_angle)
  call rotatevec(pos,(/0.,1.0,0./),y_angle)
  call rotatevec(vel,(/0.,0.,1.0/),z_angle)
  call rotatevec(vel,(/0.,1.0,0./),y_angle)

end subroutine rotate_it

!----------------------------------------------------------------
!+
!  Read disc information from discparams.list file
!+
!----------------------------------------------------------------
subroutine read_discparams(filename,R_in,R_out,H_R,R_ref,iunit,ierr)
 use infile_utils, only:open_db_from_file,inopts,read_inopt,close_db
 character(len=*), intent(in)  :: filename
 real,             intent(out) :: R_in,R_out,H_R,R_ref
 integer,          intent(in)  :: iunit
 integer,          intent(out) :: ierr
 type(inopts), allocatable :: db(:)

! Read in parameters from the file discparams.list
 call open_db_from_file(db,filename,iunit,ierr)
 if (ierr /= 0) return
 call read_inopt(R_in,'R_in',db,ierr)
 if (ierr /= 0) return
 call read_inopt(R_out,'R_out',db,ierr)
 if (ierr /= 0) return
 call read_inopt(H_R,'H/R_ref',db,ierr)
 if (ierr /= 0) return
 call read_inopt(R_ref,'R_ref',db,ierr)
 if (ierr /= 0) return
 call close_db(db)

end subroutine read_discparams

end module analysis

#!/bin/bash

export MAXP=3000000

echo "massr alpha incl sep Hor Ref"

NPAR=6; #number of parameters to pass from command line
MASSR=$1;
ALPHA=$2
INCL=$3;
SEP=$4;
HOR=$5;
NUM=$6; #Sim reference number 
SETUP="disc";

######## CATCH ERRORS #########
if [ "$#" -lt $NPAR ]; then
echo "Too few arguments.";
echo "Usage: massr alpha incl sep Hor Ref";
exit 1;
fi

MASS1="1."
MASS2=`awk 'BEGIN{print '$MASS1'*'$MASSR'*1000}'`
echo "got masses M1=$MASS1 and M2 (Jupiter masses)=$MASS2"

FOLDERNAME=""$NUM"_q"$MASSR"HR"$HOR"al"$ALPHA"Incl"$INCL"Sep"$SEP"_10orb"
FILENAME="sim$NUM"
PHANTOMFOLD="$HOME/phantom_repo"


####Message summarizing variables
echo "massratio: $MASSR; HoR: $HOR; alpha: $ALPHA; incl: $INCL sep: $SEP"

#Setting up the simulation folder
if [ ! -d $FOLDERNAME ] || [ -e ./forceit ]; then
  mkdir $FOLDERNAME;
else
  echo "This simulation already exist force, it touching a file named \"forceit\" in this folder";
  exit 1
fi

cd $FOLDERNAME;
echo "massratio: $MASSR; Incl: $INCL; HoR: $HOR; alpha: $ALPHA; sep: $SEP"> paramset.par

#copying or creating executables
if [ ! -s ../phantom ] || [ ! -s ../phantomsetup ] || [ ! -s ../phantomsetup ]
then
  $PHANTOMFOLD/scripts/writemake.sh $SETUP> Makefile;
  make;
  make setup;
  make analysis;
else
  cp ../phantom ../phantomsetup ../phantomanalysis .
fi

cp ../templ.in ./$FILENAME.in

#Personalzing the setup
sed 's/MASS2/'$MASS2'/' < ../templ.setup|sed 's/MASS1/'$MASS1'/'| sed 's/INCL/'$INCL'/'|sed 's/HOR/'$HOR'/'| sed 's/ALPHA/'$ALPHA'/'|sed 's/SEP/'$SEP'/'> "./"$FILENAME".setup"

#Creating the setup
./phantomsetup $FILENAME

#copying splash defaults in the folder
#cp ../splash.* .

#Generating submission file
gensubfDial.sh $FILENAME 36 120 > subfile.pbs
qsub subfile.pbs

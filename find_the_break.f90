! short program to identify the location of a break in the disc from angm files
! input: run in a folder with angm files, no arguments required
! parameters: three parameters can be changed: psi_crit, ibin and window (see description
! below declaration block)
! output: a list of breaks in the format of time vs. radius, in file called "breaks.txt"
! compile and run with
! gfortran -Wunused -fdefault-real-8 -fdefault-integer-8 -o find_the_break find_the_break.f90
! ./find_the_break


program find_the_break

  implicit none

  character(len=50)  :: filename,time_read
  logical :: iexist,local_minmax,local_max
  integer :: nlines, istat, iread=12, iwrite=13, ii, jj
  integer :: nfile, sigma_count,psi_count,breaks_count,window,ibin
  integer, allocatable, dimension(:) :: sigma_minima,psi_maxima
  real, allocatable, dimension(:) :: sigma, rad, psi, breaks, sigma_min_store
  real    :: dummy,lhs,rhs,time,psi_crit,sigma_max

  ! Changeable variables
  ! psi_crit: the minimum psi value to reach before a break is counted
  psi_crit = 1.0 !1.5

  ! ibin: the width over which a local min or max is defined
  ibin = 5 !10

  ! window: the number of angm bins that a min/max can be identified
  ! across - conceptually, a bit like the uncertainty in the correlation
  ! between sigma and psi
  window = 5 !3

  ! Cycle over all the angm files in the folder
  over_files: do nfile = 0,200
    write(filename,"(a4,i5.5)") 'angm',nfile

    ! Load the file
    inquire(file=filename,exist=iexist)

    if (.not.iexist) cycle over_files ! Worth checking if there are any more

    open(iread,file=filename,status='old',action='read')

    ! if it's the first file, set a couple of parameters
    if (nfile==0) then
      ! How many lines in file?
      nlines = 0
      do
          read(iread,*,iostat=istat)
          if (istat /= 0) exit
          nlines = nlines + 1
      enddo
      close(iread)
      nlines = nlines - 2   ! to account for angm headers

    endif

    ! Load the sigma and psi profiles
    allocate(sigma(nlines),rad(nlines),psi(nlines),sigma_min_store(nlines))
    allocate(sigma_minima(nlines),psi_maxima(nlines),breaks(nlines))
    open(iread,file=filename,status='old',action='read')
    read(iread,'(a)') time_read   ! for the headers
    read(iread,*)

    ! save the time from angm (horrible hack but consistent with angm format)
    read(time_read(26:44),"(es20.12)") time

    do ii=1,nlines
        read(iread,*) rad(ii), sigma(ii), dummy, dummy, dummy, &
                      dummy, dummy, dummy, psi(ii)
    enddo
    close(iread)

    ! Set the maximum initial sigma so that we have a reference for the minima
    if (nfile == 0) then
       sigma_max = maxval(sigma)
    endif

    ! Check to see if there is a minimum or maximum in sigma, using simple approximation
    ! to first derivatives.
    sigma_count = 0
    sigma_minima = 0
    do ii = ibin,nlines-ibin
      local_minmax = .false.
      lhs = (sigma(ii) - sigma(ii-1))/(rad(ii) - rad(ii-1))
      rhs = (sigma(ii+1) - sigma(ii))/(rad(ii+1) - rad(ii))
      if (lhs*rhs < 0.) local_minmax = .true.
      if (sigma(ii-ibin) > sigma(ii) .and. sigma(ii) < sigma(ii+ibin) &
          .and. local_minmax) then
        ! store location of potential local min
        sigma_count = sigma_count + 1
        sigma_minima(sigma_count) = ii
      elseif (sigma(ii-ibin) < sigma(ii) .and. sigma(ii) > sigma(ii+ibin) &
          .and. local_minmax) then
        ! store location of potential local min
        sigma_count = sigma_count + 1
        sigma_minima(sigma_count) = ii
      endif
    enddo

    ! Are there max in psi? Same test but for maxima
    psi_count = 0
    psi_maxima = 0
    do ii = ibin,nlines-ibin
      local_max = .false.
      lhs = (psi(ii) - psi(ii-1))/(rad(ii) - rad(ii-1))
      rhs = (psi(ii+1) - psi(ii))/(rad(ii+1) - rad(ii))
      if (lhs > 0. .and. rhs < 0.) local_max = .true.
      if (psi(ii-ibin) < psi(ii) .and. psi(ii) > psi(ii+ibin) &
      .and. local_max .and. psi(ii) > psi_crit) then
        ! store location of potential local min
        psi_count = psi_count + 1
        psi_maxima(psi_count) = ii
      endif
    enddo

    ! Are there any max in psi in the same region as a min in sigma?
    ! Look for correspondence within the adjacent data points
    breaks = 0
    sigma_min_store = 0.
    breaks_count = 0
    do jj = 1,psi_count
      do ii = 1,sigma_count
        if (abs(psi_maxima(jj) - sigma_minima(ii)) < window) then
          breaks_count = breaks_count + 1
          ! average these in case they don't line up perfectly (which can
          ! occur when the true value occurs in-between bins, if they agree
          ! perfectly this makes no difference)
          breaks(breaks_count) = 0.5*(rad(psi_maxima(jj))+rad(sigma_minima(ii)))
          sigma_min_store(breaks_count) = sigma(sigma_minima(ii))
        endif
      enddo
    enddo

    ! Print the result to a file
    ! Print the time, the radius it occurs at and the fraction of the initial max sigma
    if (nfile==0) then
      open(iwrite,file='breaks.txt',status="replace")
      write(iwrite,'(a)') "# time     radius     sigma      breaks"
      write(iwrite,"(es9.3 es11.3 es11.3 i3)") 0., 0.,  0., 0 ! prevents splash hissy fit
    else
      open(iwrite,file='breaks.txt',status="old",position="append")
    endif

    do ii = 1,breaks_count
      write(iwrite,"(es9.3 es11.3 es11.3 i3)") time,breaks(ii),sigma_min_store(ii)/sigma_max, breaks_count
    enddo

    ! Tidy up
    deallocate(sigma,rad,psi,sigma_minima,psi_maxima,breaks,sigma_min_store)
    close(iwrite)

  enddo over_files


end program find_the_break

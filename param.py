import numpy as np
import matplotlib.pyplot as plt

pc=3.085677E18 #cm
pcm=pc/100.
c=2.99792E5 #km/s
G=4.3E-3 #pc*(km/s)**2/Msun
# Msun=1.9884E30 #kg
# Gkgm=6.67408E-11
# pcMsunkms2=pcm*(1./Msun)*1.E3**2
# G=Gkgm/pcMsunkms2

def Rpc2Rg(Rstarpc,M):
    return Rstarpc/(G*M/c**2)

def Rg2Rpc(RstarRg,M):
    return RstarRg*(G*M/c**2)

def zeta(alpha,psi):
    return 2.*(1.+7*alpha**2)/(alpha**2*(4.+alpha**2))

def kappa(alpha,M2,Mstar,Rstar,chi,HoR,test=False):
    if test:
        zed=0.5/alpha**2
    else:
        zed=zeta(alpha,0)
    const=0.66
    return const*(M2/1.E7)**2*(chi/0.5)**2*(Mstar/1.E7)*(0.1/Rstar)**3\
                      *(0.002/HoR)**6*(0.2/alpha)**3*((1./(2.*0.2**2))/zed)**3

#to use kappa2 you needto convert Rstar in grav radii
def kappa2(alpha,q,Rstar,chi,HoR,test=False):
    if test:
        zed=0.5/alpha**2
    else:
        zed=zeta(alpha,0)
    const=0.66*(0.1*c**2/G)**3*(1./1.E7)**3*(1./0.5)**2*(0.002)**6*(0.2)**3*(1./(1./(2.*0.2**2)))**(-3)
    return const*(q)*(chi)**2*(1./Rstar)**3*(1./HoR)**6*(1./alpha)**3*(zed)**(-3)

def kappa3(q,HoR,Rstar,alpha,chi,test=False):
    if test:
        zed=0.5/alpha**2
    else:
        zed=zeta(alpha,0)

    const=2.**5/3.
    return const*q*HoR**(-6)*Rstar**(-3)*alpha**(-3)*chi**2*zed**(-3)

def R_lt(HoR,M1,chi,R0,alpha):#this result is assuming G=M=c=1
    nu0=HoR**(4./3.)*(4*chi/alpha/zeta(alpha,0))**(1./3.)#(HoR)**2*np.sqrt(R0) #See Eq.10 in Davide's paper
    return 4*chi/(alpha*nu0*zeta(alpha,0))

def R_lt2(HoR,M1,chi,R0,alpha):#this result is assuming G=M=c=1
    nu0=(HoR)**2*np.sqrt(R0) #See Eq.10 in Davide's paper
    return 4*chi/(alpha*nu0*zeta(alpha,0))


def R_ltRref(HoR,Rref,alpha,chi,l):
    #result in Rg, is the power law of (H/Rref)*(R/Rref)^l
    return (4**(2./3.)*HoR**(-4./3.)*alpha**(-2./3.)*zeta(alpha,0)**(-2./3.)\
           *chi**(2./3.)*Rref**(4.*l/3.))**(1./(1.+4./3.*l))

def R_tid(HoR,M1,chi,R0,alpha,Rstar,q):
    nu0=(HoR)**2*np.sqrt(R0)
    return (3./2./q*Rstar**3*alpha*nu0*zeta(alpha,0))**(2./7.)

def R_tid2(HoR,M1,chi,R0,alpha,Rstar,q):
    nu0=HoR**(4./3.)*(4.*chi/alpha/zeta(alpha,0))**(1./3.)
    return (3./2./q*Rstar**3*alpha*nu0*zeta(alpha,0))**(2./7.)

def R_tid3(HoR,Rstar,alpha,chi,q,test=False):
    if test:
        zed=0.5/alpha**2
    else:
        zed=zeta(alpha,0)
    return (3./2.**(1./3.)*q**(-1.)*Rstar**(3.)*HoR**(4./3.)*alpha**(2./3.)*chi**(1./3.)*zed**(2./3.))**(2./7.)

def H_0R_0(HoR,R0,Rref,q):
    return HoR*(R0/Rref)**q

if __name__=="__main__":

#to calculate R_LT all methods are equivalent. It is important to keep in mind though that R_lt1 and R_lt2
# use H_0/R_0=(H/R)(R_lt), i.e. calculated are R_lt. R_ltRef, calculates 
#it with any choice of R_ref and H/R (for this reason you also need "l").
# IN GENERAL USE R_ltRref as it works everytime: i.e. also if you choose Rref=R_lt. 
#IMPORTANT: all the other equtions need to use H_0/R_0 defined at R_lt so, after calculating R_ltRref you
#need to use H_0R_0(HoR,R0,Rref,q)

#R_tid3 is the one that I recalculated independently. The other ones do not provide the right result

#same applies to kappa3, which gives the same result as R_ltRref/R_tid3 

#perche cambio di un fattore 100 alla costante non shifta tutto il plot. il problema e' che cambia R_lt e di conseguenza cambia H/R_0 in modo molto non lineare. col risultato che si imputtana tutto! 

    M=1.E7
    Rstarpc=0.1
    Rsg=Rpc2Rg(Rstarpc,M)

    print "tests, Rsg=",Rsg
    print kappa(0.2,1.E7,1.E7,Rstarpc,0.5,0.002,test=True)
    print kappa(0.2,1.E7,1.E7,Rstarpc,0.5,0.002)
    print kappa2(0.2,1,Rsg,0.5,0.002,test=True)
    print kappa2(0.2,1,Rsg,0.5,0.002)
    print kappa3(1.,0.002,Rsg,0.2,0.5,test=True)
    print kappa3(1.,0.002,Rsg,0.2,0.5)


    q=-0.25 #H/R=H_0/R_0*(R/R_0)^q
    print "Compare with std parameters in Eq. (21) and (22) of Gerosa et al. "
    R_ltcorr=R_ltRref(0.05,30,0.1,0.9,-0.25)
    print "R_lt is:",R_ltcorr
    HoR0=H_0R_0(0.05,R_ltcorr,30,-0.25)
    print "H_0/R_0",HoR0

    print "in pc with M=1.E7:",Rg2Rpc(R_ltcorr,1.E7)

    print "Kappa1 for your params:", kappa(0.10,1.E7,1.E5,Rg2Rpc(398,1.E7),0.9,HoR0)
    print "Kappa2 for your params:", kappa2(0.10,0.01,398,0.9,HoR0)
    print "Kappa3 for your params:", kappa3(0.01,HoR0,398.,0.1,0.9)
    R_ltN=R_lt(HoR0,1,0.9,R_ltcorr,0.1)
    R_ltN2=R_lt2(HoR0,1,0.9,R_ltcorr,0.1)
    R_ltcorr=R_ltRref(HoR0,R_ltcorr,0.1,0.9,-0.25)
    R_tidN=R_tid(HoR0,1.,0.9,R_ltcorr,0.1,398,0.01)
    R_tidN2=R_tid2(HoR0,1.,0.9,R_ltcorr,0.1,398,0.01)
    R_tidN3=R_tid3(HoR0,398,0.1,0.9,0.01)
    print "R_lt:", R_ltN
    print "R_ltcorr",R_ltcorr
    print "R_lt2",R_ltN2
    print "R_tid:",R_tidN
    print "R_tid2,R_tid3:",R_tidN2,R_tidN3
    print "(R_tid/R_lt)^(-7/2)=kappa:",(R_tidN/R_ltN)**(-7./2.)
    print "(R_tid/R_ltcorr)^(-7/2)=kappa:",(R_tidN/R_ltcorr)**(-7./2.)
    print "(R_tid2/R_ltcorr)^(-7/2)=kappa:",(R_tidN2/R_ltcorr)**(-7./2.)

    print ""
    print "new tests (these are right!):"
    print "(R_tid/R_lt)^(-7/2)=kappa:",(R_ltcorr/R_tidN3)**(7./2.)
    print "kappa3:", kappa3(0.01,HoR0,398.,0.1,0.9)

    paramsA=np.array([0.1,0.01,398,0.9,0.05]) #i.e. alpha,q,a_bin,BHspin,H/R
    paramsB=np.array([0.15,0.01,398,0.9,0.05])
    paramsC=np.array([0.15,0.01,250,0.9,0.01])
    paramsD=np.array([0.10,0.01,250,0.9,0.01])
    paramsBecE=np.array([0.05,0.01,399,0.9,0.05])
    paramsBecF=np.array([0.05,0.01,250,0.9,0.01])
    paramsBecG=np.array([0.2,0.01,250,0.9,0.01])
    names=['A','B','C','D','E','F','G']

    data005=np.loadtxt("breakAlpha005.dat")
    data01=np.loadtxt("breakAlpha01.dat")
    data015=np.loadtxt("breakAlpha015.dat")
    data02=np.loadtxt('breakAlpha02.dat')

    i=0
    kappas=[]
    breaksim=[]
    namefiles=['breakingA.txt','breakingB.txt','breakingC.txt','breakingD.txt',\
                'breakingE.txt','breakingF.txt','breakingG.txt']
    colouralpha=['blue','green','green','blue','red','red','orange']
    for param in [paramsA,paramsB,paramsC,paramsD,paramsBecE,paramsBecF,paramsBecG]:
        R_ltcorr=R_ltRref(param[4],30,param[0],param[3],-0.25)
        HoR0=H_0R_0(param[4],R_ltcorr,30,-0.25)
        R_tidN=R_tid3(HoR0,param[2],param[0],param[3],param[1],test=False)
        kappareal=(R_tidN/R_ltcorr)**(-7./2.)
        kappafake=kappa3(param[1],HoR0,param[2],param[0],param[3],test=False)
        print "Case"+names[i]+', kappareal, kappafake, ratio,HoR0, Rcorr, R_tid:',kappareal,kappafake,kappareal/kappafake,HoR0,R_ltcorr,R_tidN
        kappas.append(kappareal)
        breaksim.append(np.loadtxt(namefiles[i],dtype=str))
        i=i+1


    plt.plot(data005[:,0],data005[:,1],label='$\\alpha_{\\rm ss}=0.05$',color='red')
    plt.plot(data01[:,0],data01[:,1],label='$\\alpha_{\\rm ss}=0.10$',color='blue')
    plt.plot(data015[:,0],data015[:,1],label='$\\alpha_{\\rm ss}=0.15$',color='green')
    plt.plot(data02[:,0],data02[:,1],label='$\\alpha_{\\rm ss}=0.2$',color='orange')
    plt.plot(data005[:,0],180.-data005[:,1],color='red')
    plt.plot(data01[:,0],180.-data01[:,1],color='blue')
    plt.plot(data015[:,0],180.-data015[:,1],color='green')
    plt.plot(data02[:,0],180.-data02[:,1],color='orange')
    plt.legend()
    plt.xscale('log')
    plt.xlabel('$\\kappa$')
    plt.ylabel('Spin Angle ($^\\circ$)')
    i=0
    for sim in breaksim:
        kappaval=np.ones(len(sim[:,0]))*kappas[i]
        j=0
        for ka in kappaval:
            plt.scatter(ka,float(sim[j,0]),marker=sim[j,2],color=colouralpha[i])
            j=j+1
        i=i+1
